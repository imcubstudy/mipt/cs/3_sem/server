#include <errno.h>
#include <stdlib.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <fcntl.h>
#include <unistd.h>

#include <math.h>

#define _str(x) #x
#define str(x) _str(x)
#define $(code...)                                                  \
    do {                                                            \
        errno = 0;                                                  \
        code;                                                       \
        if(errno != 0) {                                            \
            perror(#code " at " str(__LINE__));                     \
            exit(EXIT_FAILURE);                                     \
        }                                                           \
    } while(0)

#define TEST(cond, msg)                                             \
    do {                                                            \
        if(!(cond)) {                                               \
            fprintf(stderr, msg "\n");                              \
            fflush(stderr);                                         \
            exit(EXIT_FAILURE);                                     \
        }                                                           \
    } while(0)

#define min(x, y)                                                   \
    ({                                                              \
        typeof(x) __x = (x);                                        \
        typeof(y) __y = (y);                                        \
        __x < __y ? __x : __y;                                      \
    })

#define max(x, y)                                                   \
    ({                                                              \
        typeof(x) __x = (x);                                        \
        typeof(y) __y = (y);                                        \
        __x > __y ? __x : __y;                                      \
    })

#define swap(x, y)                                                          \
    do {                                                                    \
        typeof(x) _tmp = (x);                                               \
        x = (y);                                                            \
        y = _tmp;                                                           \
    } while(0)

#define bufsize(N, i)   (min(1024 * pow(3, N - i),  ((1 << 7) << 10)))
#define CL_BUFSZ        4096

typedef struct pipe_t {
    int rfd;
    int wfd;
} pipe_t;

typedef struct con_t {
    pipe_t  src;
    pipe_t  dst;

    int     bufsz;
    void*   buf;
    int     npendb;
    int     bufsh;

    int     eof;
} con_t;

long parseint(const char* _nptr);

void client(con_t _con);
void server(con_t *_conbuf, int _ncon);

con_t *init_connections(int N);

int main(int argc, char* const argv[])
{
    TEST(argc == 3, "N and path not specified");

    long N = -1;
    $(N = parseint(argv[1]));

    int src_fd = -1;
    $(src_fd = open(argv[2], O_RDONLY));
    $(dup2(src_fd, STDIN_FILENO));

    con_t* conbuf = NULL;
    $(conbuf = init_connections(N));

    server(conbuf, N + 2);

    return 0;
}

long parseint(const char* _nptr)
{
    char* eptr = NULL;
    errno = 0;

    long res = strtol(_nptr, &eptr, 0);
    if(errno != 0 || *eptr != '\0') {
        errno = EINVAL;
        return -1;
    }
    return res;
}

pipe_t getpipe()
{
    int fds[2] = {};
    $(pipe(fds));
    pipe_t res = { .rfd = fds[0],
                   .wfd = fds[1] };
    return res;
}

con_t *init_connections(int N)
{
    con_t *buf = (con_t*)calloc(N + 2, sizeof(con_t));
    TEST(buf != NULL, "ENOMEM");

    for(int i = 1; i <= N; ++i) {
        buf[i].src = getpipe();
        buf[i].dst = getpipe();
    }

    for(int i = 1; i <= N; ++i) {
        switch(fork()) {
            case -1:
                TEST(!errno, "Fork error");
                break;
            case 0:
                swap(buf[i].src, buf[i].dst);

                for(int j = 1; j <= N; ++j) {
                    close(buf[j].src.wfd);
                    close(buf[j].dst.rfd);

                    if(i != j) {
                        $(close(buf[j].src.rfd));
                        $(close(buf[j].dst.wfd));
                    }
                }

                con_t con = buf[i];
                $(free(buf));
                client(con);

                break;

            default:
                buf[i].bufsz = bufsize(N, i);
                $(buf[i].buf = calloc(buf[i].bufsz, 1));

                $(close(buf[i].dst.rfd));
                $(close(buf[i].src.wfd));

                $(fcntl(buf[i].dst.wfd, F_SETFL, O_WRONLY | O_NONBLOCK));
                $(fcntl(buf[i].src.rfd, F_SETFL, O_RDONLY | O_NONBLOCK));

                break;
        }
    }

    buf[0].src.rfd = STDIN_FILENO;

    buf[N + 1].bufsz = bufsize(N, N + 1);
    $(buf[N + 1].buf = calloc(buf[N + 1].bufsz, 1));
    buf[N + 1].dst.wfd = STDOUT_FILENO;

    return buf;
}

void server(con_t *_conbuf, int _ncon)
{
    do {
        fd_set rfds, wfds;
        FD_ZERO(&rfds);
        FD_ZERO(&wfds);

        int mfd = -1;
        for(int i = 0; i < _ncon - 1; ++i)
            if(!_conbuf[i].eof && _conbuf[i + 1].npendb == 0) {
                FD_SET(_conbuf[i].src.rfd, &rfds);
                mfd = max(mfd, _conbuf[i].src.rfd);
            }

        for(int i = 0; i < _ncon; ++i)
            if(_conbuf[i].npendb != 0) {
                FD_SET(_conbuf[i].dst.wfd, &wfds);
                mfd = max(mfd, _conbuf[i].dst.wfd);
            }

        if(mfd == -1) break;
        $(select(mfd + 1, &rfds, &wfds, NULL, NULL));

        for(int i = 0; i < _ncon; ++i) {
            if(i != 0 && FD_ISSET(_conbuf[i - 1].src.rfd, &rfds)) {
                int nbytes = read(_conbuf[i - 1].src.rfd, _conbuf[i].buf,
                                                          _conbuf[i].bufsz);
                TEST(nbytes != -1, "read error");

                _conbuf[i].npendb += nbytes;

                if(nbytes == 0) {
                    $(close(_conbuf[i - 1].src.rfd));
                    $(close(_conbuf[i].dst.wfd));
                    _conbuf[i - 1].eof = 1;
                }
            }
            if(FD_ISSET(_conbuf[i].dst.wfd, &wfds)) {
                int nbytes = write(_conbuf[i].dst.wfd,
                        _conbuf[i].buf + _conbuf[i].bufsh, _conbuf[i].npendb);
                TEST(nbytes != -1, "write error");

                _conbuf[i].npendb -= nbytes;
                _conbuf[i].bufsh  += nbytes;

                if(_conbuf[i].npendb == 0)
                    _conbuf[i].bufsh = 0;
            }
        }
    } while(1);

    for(int i = 1; i < _ncon; ++i)
        $(free(_conbuf[i].buf));
    $(free(_conbuf));

    $(exit(EXIT_SUCCESS));
}

void client(con_t _con)
{
    char buf[CL_BUFSZ] = {};
    int ret = 0;
    do {
        $(ret = read(_con.src.rfd, buf, CL_BUFSZ));
        if(write(_con.dst.wfd, buf, ret) != ret) {
            perror("child write");
            exit(EXIT_FAILURE);
        }
    } while(ret);

    $(close(_con.src.rfd));
    $(close(_con.dst.wfd));

    $(exit(EXIT_SUCCESS));
}

